package org.lox;

import java.util.List;
import java.util.Optional;

public interface Stmt {
    public static record Expression(Expr expression) implements Stmt {
    }

    public static record Print(Expr expression) implements Stmt {
    }

    public static record Var(Token name, Expr initialiser) implements Stmt {
    }

    public static record Block(List<Stmt> stmts) implements Stmt {
    }

    public static record If(Expr condition, Stmt trueStmt, Optional<Stmt> falseStmt) implements Stmt {
    }

    public static record Loop(Expr condition, Stmt stmt) implements Stmt {
    }
}
