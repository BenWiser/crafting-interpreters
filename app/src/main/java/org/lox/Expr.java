package org.lox;

// The book uses meta programming for this but
// Java records have since made it super easy to
// represent this stuff.
public interface Expr {
    public static record Binary(Expr left, Token operator, Expr right) implements Expr {
    }

    public static record Logical(Expr left, Token operator, Expr right) implements Expr {
    }

    public static record Grouping(Expr expressions) implements Expr {
    }

    public static record Literal(Object value) implements Expr {
    }

    public static record Unary(Token operator, Expr right) implements Expr {
    }

    public static record Variable(Token name) implements Expr {
    }

    public static record Assignment(Token name, Expr value) implements Expr {
    }
}
