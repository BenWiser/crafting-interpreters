package org.lox;

public class Token {
    final TokenType type;
    final String lexeme;
    final Object literal;
    final int line;

    public Token(TokenType type, String lexem, Object literal, int line) {
        this.type = type;
        this.lexeme = lexem;
        this.literal = literal;
        this.line = line;
    }

    @Override
    public String toString() {
        return type + " " + lexeme + " " + literal;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Token token) {
            return toString().equals(token.toString());
        }
        return false;
    }
}
