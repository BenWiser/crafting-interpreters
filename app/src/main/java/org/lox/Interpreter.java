package org.lox;

import java.util.List;

public class Interpreter {
    private Environment environment = new Environment();

    public interface Printer {
        public void print(Object expr);
    }

    private final Printer mPrinter;

    public Interpreter(Printer printer) {
        mPrinter = printer;
    }

    public void interpret(List<Stmt> stmts) {
        for (Stmt stmt : stmts) {
            try {
                execute(stmt);
            } catch (Interpreter.RuntimeError err) {
                JLox.runtimeError(err);
            }
        }
    }

    private void execute(Stmt stmt) {
        switch (stmt) {
            case Stmt.Expression expr -> expressionStmt(expr);
            case Stmt.Print print -> printStmt(print);
            case Stmt.Var st -> varStmt(st);
            case Stmt.Block block -> blockStmt(block);
            case Stmt.If st -> ifStmt(st);
            case Stmt.Loop st -> loopStmt(st);
            default -> {
                // Unreachable
            }
        }
    }

    private void expressionStmt(Stmt.Expression stmt) {
        evaluate(stmt.expression());
    }

    private void printStmt(Stmt.Print stmt) {
        var result = evaluate(stmt.expression());
        mPrinter.print(stringify(result));
    }

    private void varStmt(Stmt.Var stmt) {
        environment.define(stmt.name().lexeme, evaluate(stmt.initialiser()));
    }

    private void blockStmt(Stmt.Block block) {
        environment = new Environment(environment);

        try {
            interpret(block.stmts());
        } finally {
            // We always need to return to the parent environment
            environment = environment.mEnclosing;
        }
    }

    private void ifStmt(Stmt.If stmt) {
        if (isTruthy(evaluate(stmt.condition()))) {
            execute(stmt.trueStmt());
        } else if (stmt.falseStmt().isPresent()) {
            execute(stmt.falseStmt().get());
        }
    }

    private void loopStmt(Stmt.Loop st) {
        while (isTruthy(evaluate(st.condition()))) {
            execute(st.stmt());
        }
    }

    private Object evaluate(Expr expr) {
        return switch (expr) {
            case Expr.Binary ex -> binary(ex);
            case Expr.Logical ex -> logical(ex);
            case Expr.Grouping ex -> grouping(ex);
            case Expr.Literal ex -> literal(ex);
            case Expr.Unary ex -> unary(ex);
            case Expr.Variable ex -> variable(ex);
            case Expr.Assignment ex -> assignment(ex);
            default -> null;
        };
    }

    private Object logical(Expr.Logical ex) {
        Object left = evaluate(ex.left());

        // If it is an or operator, we can stop early if
        // the left is true.
        // We can also stop early if the left was false for
        // an or operator.
        if (ex.operator().type == TokenType.OR) {
            if (isTruthy((left)))
                return left;
        } else {
            if (!isTruthy(left))
                return left;
        }

        // Otherwise we need to keep going.
        return evaluate(ex.right());
    }

    private Object binary(Expr.Binary expr) {
        Object left = evaluate(expr.left());
        Object right = evaluate(expr.right());

        return switch (expr.operator().type) {
            case TokenType.MINUS -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left - (double) right;
            }
            case TokenType.PLUS -> {
                if (left instanceof Double && right instanceof Double) {
                    yield (double) left + (double) right;
                }
                if (left instanceof String || right instanceof String) {
                    yield stringify(left) + stringify(right);
                }

                throw new RuntimeError(expr.operator(),
                        "Operands must be two numbers or string concatination.");
            }
            case TokenType.STAR -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left * (double) right;
            }
            case TokenType.SLASH -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left / (double) right;
            }

            case TokenType.GREATER -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left > (double) right;
            }
            case TokenType.GREATER_EQUAL -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left >= (double) right;
            }
            case TokenType.LESS -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left < (double) right;
            }
            case TokenType.LESS_EQUAL -> {
                checkNumberOperands(expr.operator(), left, right);
                yield (double) left <= (double) right;
            }

            case TokenType.EQUAL_EQUAL -> isEqual(left, right);
            case TokenType.BANG_EQUAL -> !isEqual(left, right);

            default -> null;
        };
    }

    private Object grouping(Expr.Grouping expr) {
        return evaluate(expr.expressions());
    }

    private Object literal(Expr.Literal expr) {
        return expr.value();
    }

    private Object unary(Expr.Unary expr) {
        Object right = evaluate(expr.right());

        return switch (expr.operator().type) {
            case TokenType.MINUS -> {
                checkNumberOperand(expr.operator(), right);
                yield (double) right;
            }
            case TokenType.BANG -> !isTruthy(right);

            // Unreachable
            default -> null;
        };
    }

    private Object variable(Expr.Variable expr) {
        return environment.get(expr.name());
    }

    private Object assignment(Expr.Assignment expr) {
        Object value = evaluate(expr.value());
        environment.assign(expr.name(), value);
        return value;
    }

    private boolean isEqual(Object left, Object right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null) {
            return false;
        }
        return left.equals(right);
    }

    private boolean isTruthy(Object val) {
        if (val == null) {
            return false;
        }
        if (val instanceof Boolean) {
            return (boolean) val;
        }

        return true;
    }

    private void checkNumberOperands(Token operator, Object... values) {
        for (Object value : values) {
            if (!(value instanceof Double))
                throw new RuntimeError(operator, "Operands must be numbers.");
        }
    }

    private void checkNumberOperand(Token operator, Object operand) {
        if (operand instanceof Double)
            return;
        throw new RuntimeError(operator, "Operand must be a number.");
    }

    private static String stringify(Object obj) {
        if (obj == null)
            return "nil";

        String text = obj.toString();

        // Remove trailing decimals if whole number since
        // lox treats ints as doubles internally as well.
        if (obj instanceof Double && text.endsWith(".0")) {
            text = text.substring(0, text.length() - 2);
        }

        return text;
    }

    public static class RuntimeError extends RuntimeException {
        final Token token;

        RuntimeError(Token token, String message) {
            super(message);
            this.token = token;
        }
    }
}
