package org.lox;

// The original book used the visitor pattern but
// Java 17s pattern matching makes this a lot easier.
public class AstPrinter {
    public String print(Expr expr) {
        return switch (expr) {
            case Expr.Binary ex -> printBinary(ex);
            case Expr.Grouping ex -> printGrouping(ex);
            case Expr.Literal ex -> printLiteral(ex);
            case Expr.Unary ex -> printUnary(ex);
            default -> "";
        };
    }

    String printBinary(Expr.Binary binary) {
        return parenthesize(
                binary.operator().lexeme, binary.left(), binary.right());
    }

    String printGrouping(Expr.Grouping grouping) {
        return parenthesize("group", grouping.expressions());
    }

    String printLiteral(Expr.Literal literal) {
        if (literal.value() == null)
            return "nil";

        return literal.value().toString();
    }

    String printUnary(Expr.Unary unary) {
        return parenthesize(unary.operator().lexeme, unary.right());
    }

    private String parenthesize(String name, Expr... exprs) {
        StringBuilder builder = new StringBuilder();

        builder.append("(").append(name);
        for (Expr expr : exprs) {
            builder.append(" ");
            builder.append(print(expr));
        }
        builder.append(")");

        return builder.toString();
    }
}
