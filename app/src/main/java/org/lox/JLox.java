package org.lox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JLox {
    private static boolean sHadError;
    private static boolean sHadRuntimeError;
    private static final Interpreter mInterpreter = new Interpreter((Object expr) -> {
        System.out.println(expr);
    });

    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            System.out.println("Usage: jlox [script]");
            System.exit(64);
        } else if (args.length == 1) {
            runFile(args[0]);
        } else {
            runPrompt();
        }
    }

    static void runFile(String file) throws IOException {
        var data = Files.readAllBytes(Paths.get(file));
        run(new String(data));

        if (sHadError)
            System.exit(65);

        if (sHadRuntimeError)
            System.exit(70);
    }

    static void runPrompt() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            System.out.print("> ");
            var line = reader.readLine();
            if (line == null)
                break;
            run(line);

            sHadError = false;
        }
    }

    public static void run(String script) {
        var scanner = new Scanner(script);
        var tokens = scanner.scanTokens();

        Parser parser = new Parser(tokens);
        var stmts = parser.parse();

        if (sHadError)
            return;

        // The book does this in the interpreter but that
        // makes it a little less easy to write tests for.
        // I instead opted to expose the runtime error and deal
        // with it here so that the test class can verify the results.
        try {
            mInterpreter.interpret(stmts);
        } catch (Interpreter.RuntimeError error) {
            runtimeError(error);
        }
    }

    static void reportError(int line, String message) {
        report(line, "", message);
    }

    static void reportError(Token token, String message) {
        if (token.type == TokenType.EOF) {
            report(token.line, " at end", message);
        } else {
            report(token.line, " at '" + token.lexeme + "'", message);
        }
    }

    static void runtimeError(Interpreter.RuntimeError error) {
        System.err.println(String.format("[line %d] %s", error.token.line, error.getMessage()));
        sHadRuntimeError = true;
    }

    private static void report(int line, String where, String message) {
        System.err.println(String.format("[line: %d] Error %s: %s", line, where, message));
        sHadError = true;
    }
}
