package org.lox;

import java.util.HashMap;
import java.util.Map;

public class Environment {
    final Environment mEnclosing;
    private final Map<String, Object> values = new HashMap<>();

    public Environment() {
        this.mEnclosing = null;
    }

    public Environment(Environment enclosing) {
        this.mEnclosing = enclosing;
    }

    public void define(String name, Object value) {
        values.put(name, value);
    }

    public void assign(Token name, Object value) {
        if (values.containsKey(name.lexeme)) {
            values.put(name.lexeme, value);
            return;
        }

        if (this.mEnclosing != null) {
            mEnclosing.assign(name, value);
            return;
        }

        throw new Interpreter.RuntimeError(name,
                String.format("Undefined variable '%s'.", name.lexeme));
    }

    public Object get(Token name) {
        if (values.containsKey(name.lexeme)) {
            return values.get(name.lexeme);
        }
        if (mEnclosing != null) {
            return mEnclosing.get(name);
        }

        throw new Interpreter.RuntimeError(name,
                String.format("Undefined variable '%s'.", name.lexeme));
    }
}
