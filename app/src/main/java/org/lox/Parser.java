package org.lox;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

public class Parser {
    private final List<Token> tokens;
    private int current = 0;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    public List<Stmt> parse() {
        var statements = new ArrayList<Stmt>();

        while (!isAtEnd())
            statements.add(declaration());

        return statements;
    }

    private Stmt declaration() {
        try {
            if (match(TokenType.VAR))
                return varStatement();

            return statement();
        } catch (ParseError err) {
            synchronize();
            return null;
        }
    }

    private Stmt statement() {
        if (match(TokenType.LEFT_BRACE))
            return blockStatement();
        if (match(TokenType.PRINT))
            return printStatement();
        if (match(TokenType.IF))
            return ifStatement();
        if (match(TokenType.LOOP))
            return loopStatement();

        return expressionStatement();
    }

    private Stmt blockStatement() {
        var stmts = new ArrayList<Stmt>();

        while (!isAtEnd() && !check(TokenType.RIGHT_BRACE)) {
            stmts.add(declaration());
        }

        consume(TokenType.RIGHT_BRACE, "Expected to close scope with '}'");

        return new Stmt.Block(stmts);
    }

    private Stmt printStatement() {
        var stmt = new Stmt.Print(expression());
        consume(TokenType.SEMICOLON, "Expected ';' after print.");
        return stmt;
    }

    private Stmt ifStatement() {
        consume(TokenType.LEFT_PAREN, "Expected '(' after if statement");
        var condition = expression();
        consume(TokenType.RIGHT_PAREN, "Expected '{' after if statement");
        var trueStatement = statement();
        Optional<Stmt> falseStatment = Optional.empty();

        if (match(TokenType.ELSE)) {
            falseStatment = Optional.of(statement());
        }

        return new Stmt.If(condition, trueStatement, falseStatment);
    }

    private Stmt loopStatement() {
        consume(TokenType.LEFT_PAREN, "Expected '(' after if statement");
        var condition = expression();
        consume(TokenType.RIGHT_PAREN, "Expected '{' after if statement");

        var stmt = statement();

        return new Stmt.Loop(condition, stmt);
    }

    private Stmt varStatement() {
        var name = consume(TokenType.IDENTIFIER, "Expected variable name");
        // If the var initialiser wasn't provided, set to null
        Expr init = new Expr.Literal(null);
        if (match(TokenType.EQUAL)) {
            init = expression();
        }

        consume(TokenType.SEMICOLON, "Expected ';' after var.");

        return new Stmt.Var(name, init);
    }

    private Stmt expressionStatement() {
        var stmt = new Stmt.Expression(expression());
        consume(TokenType.SEMICOLON, "Expected ';' after statement.");
        return stmt;
    }

    private Expr expression() {
        return assignment();
    }

    private Expr assignment() {
        Expr expr = or();

        if (match(TokenType.EQUAL)) {
            Token equals = previous();
            Expr value = or();

            if (expr instanceof Expr.Variable variable) {
                return new Expr.Assignment(variable.name(), value);
            }

            error(equals, "Invalid assignment target.");
        }

        return expr;
    }

    private Expr or() {
        Expr expr = and();

        while (match(TokenType.OR)) {
            Token operator = previous();
            Expr right = and();

            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr and() {
        Expr expr = equality();

        while (match(TokenType.AND)) {
            Token operator = previous();
            Expr right = equality();

            expr = new Expr.Logical(expr, operator, right);
        }

        return expr;
    }

    private Expr equality() {
        Expr expr = comparison();

        while (match(TokenType.BANG_EQUAL, TokenType.EQUAL_EQUAL)) {
            Token operator = previous();
            Expr right = comparison();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr comparison() {
        Expr expr = term();

        while (match(TokenType.GREATER, TokenType.GREATER_EQUAL, TokenType.LESS, TokenType.LESS_EQUAL)) {
            Token operator = previous();
            Expr right = term();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr term() {
        Expr expr = factor();

        while (match(TokenType.MINUS, TokenType.PLUS)) {
            Token operator = previous();
            Expr right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr factor() {
        Expr expr = unary();

        while (match(TokenType.SLASH, TokenType.STAR)) {
            Token operator = previous();
            Expr right = unary();
            expr = new Expr.Binary(expr, operator, right);
        }

        return expr;
    }

    private Expr unary() {
        if (match(TokenType.BANG, TokenType.MINUS)) {
            Token operator = previous();
            Expr right = unary();
            return new Expr.Unary(operator, right);
        }

        return primary();
    }

    private Expr primary() {
        if (match(TokenType.TRUE))
            return new Expr.Literal(true);
        if (match(TokenType.FALSE))
            return new Expr.Literal(false);
        if (match(TokenType.NIL))
            return new Expr.Literal(null);
        if (match(TokenType.NUMBER, TokenType.STRING))
            return new Expr.Literal(previous().literal);
        if (match(TokenType.IDENTIFIER))
            return new Expr.Variable(previous());

        if (match(TokenType.LEFT_PAREN)) {
            Expr expr = expression();
            consume(TokenType.RIGHT_PAREN, "Expected ')' after expression");
            return new Expr.Grouping(expr);
        }

        throw error(peek(), "Expect expression.");
    }

    private boolean match(TokenType... types) {
        for (TokenType type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }

        return false;
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private boolean check(TokenType type) {
        if (isAtEnd())
            return false;
        return peek().type == type;
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token advance() {
        if (!isAtEnd())
            current++;
        return previous();
    }

    private boolean isAtEnd() {
        return peek().type == TokenType.EOF;
    }

    private Token consume(TokenType type, String error) {
        if (check(type)) {
            return advance();
        }

        throw error(peek(), error);
    }

    private void synchronize() {
        advance();

        while (!isAtEnd()) {
            if (previous().type == TokenType.SEMICOLON)
                return;

            switch (peek().type) {
                case TokenType.CLASS:
                case TokenType.FUN:
                case TokenType.VAR:
                case TokenType.FOR:
                case TokenType.IF:
                case TokenType.LOOP:
                case TokenType.PRINT:
                case TokenType.RETURN:
                    return;

                default:
                    advance();
            }
        }
    }

    private ParseError error(Token token, String error) {
        JLox.reportError(token, error);
        return new ParseError();
    }

    public static class ParseError extends RuntimeException {
    }
}
