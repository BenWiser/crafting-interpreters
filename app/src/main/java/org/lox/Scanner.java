package org.lox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scanner {
    private final String source;
    private final List<Token> tokens;
    private int start = 0;
    private int current = 0;
    private int line = 1;

    private static final Map<String, TokenType> sKeyWords;

    static {
        sKeyWords = new HashMap<>();
        sKeyWords.put("and", TokenType.AND);
        sKeyWords.put("or", TokenType.OR);
        sKeyWords.put("else", TokenType.ELSE);
        sKeyWords.put("true", TokenType.TRUE);
        sKeyWords.put("false", TokenType.FALSE);
        sKeyWords.put("fun", TokenType.FUN);
        sKeyWords.put("if", TokenType.IF);
        sKeyWords.put("for", TokenType.FOR);
        sKeyWords.put("nil", TokenType.NIL);
        sKeyWords.put("class", TokenType.CLASS);
        sKeyWords.put("print", TokenType.PRINT);
        sKeyWords.put("return", TokenType.RETURN);
        sKeyWords.put("super", TokenType.SUPER);
        sKeyWords.put("this", TokenType.THIS);
        sKeyWords.put("var", TokenType.VAR);
        sKeyWords.put("loop", TokenType.LOOP);
    }

    public Scanner(String source) {
        this.source = source;
        this.tokens = new ArrayList<>();
    }

    public List<Token> scanTokens() {

        while (!isAtEnd()) {
            start = current;
            scanToken();
        }

        tokens.add(new Token(TokenType.EOF, "", null, line));

        return tokens;
    }

    private boolean isAtEnd() {
        return current >= source.length();
    }

    private char advance() {
        return source.charAt(current++);
    }

    private void scanToken() {
        var c = advance();
        switch (c) {
            case '(':
                addToken(TokenType.LEFT_PAREN);
                break;
            case ')':
                addToken(TokenType.RIGHT_PAREN);
                break;
            case '{':
                addToken(TokenType.LEFT_BRACE);
                break;
            case '}':
                addToken(TokenType.RIGHT_BRACE);
                break;
            case ',':
                addToken(TokenType.COMMA);
                break;
            case '.':
                addToken(TokenType.DOT);
                break;
            case '-':
                addToken(TokenType.MINUS);
                break;
            case '+':
                addToken(TokenType.PLUS);
                break;
            case ';':
                addToken(TokenType.SEMICOLON);
                break;
            case '*':
                addToken(TokenType.STAR);
                break;
            case '!':
                addToken(match('=') ? TokenType.BANG_EQUAL : TokenType.BANG);
                break;
            case '=':
                addToken(match('=') ? TokenType.EQUAL_EQUAL : TokenType.EQUAL);
                break;
            case '<':
                addToken(match('=') ? TokenType.LESS_EQUAL : TokenType.LESS);
                break;
            case '>':
                addToken(match('=') ? TokenType.GREATER_EQUAL : TokenType.GREATER);
                break;

            case '/':
                if (match('/')) {
                    while (peak() != '\n' && !isAtEnd()) {
                        // Just keep skipping til we are past the comment
                        advance();
                    }
                } else if (match('*')) {
                    // Multiline so keep going until found end
                    while (!isAtEnd() && peak() != '*' && peakNext() != '/') {
                        advance();

                        if (peak() == '\n') {
                            line++;
                        }
                    }

                    if (isAtEnd())
                        JLox.reportError(line, "Multiline comment incomplete");

                    // Advance twice since we matched the next two
                    advance();
                    advance();
                } else {
                    addToken(TokenType.SLASH);
                }

                break;

            case '\n':
                line++;
                // Onwards!
                break;

            case '"':
                string();
                break;

            case '\t':
            case ' ':
            case '\r':
                // bye bye whitespace
                break;

            default:
                if (isDigit(c)) {
                    number();
                    break;
                }
                if (isAlpha(c)) {
                    identifer();
                    break;
                }
                JLox.reportError(line, "Unexpected character");
                break;
        }
    }

    private void addToken(TokenType type) {
        addToken(type, null);
    }

    private void addToken(TokenType type, Object literal) {
        String text = source.substring(start, current);
        tokens.add(new Token(type, text, literal, line));
    }

    private boolean match(char expected) {
        if (isAtEnd())
            return false;
        if (source.charAt(current) != expected)
            return false;

        current++;
        return true;
    }

    char peak() {
        if (isAtEnd())
            return '\0';

        return source.charAt(current);
    }

    char peakNext() {
        if (current + 1 >= source.length()) {
            return '\0';
        }
        return source.charAt(current + 1);
    }

    void string() {
        while (peak() != '"' && !isAtEnd()) {
            // Lox allows multiline strings
            if (peak() == '\n')
                line++;
            advance();
        }

        if (isAtEnd()) {
            JLox.reportError(line, "Unterminated string");
            return;
        }

        // This means we finished the "
        advance();

        // Remember to strip the quotes from the string literal
        addToken(TokenType.STRING, source.substring(start + 1, current - 1));
    }

    void identifer() {
        while (isAlphaNumeric(peak()))
            advance();

        String text = source.substring(start, current);
        addToken(sKeyWords.getOrDefault(text, TokenType.IDENTIFIER));
    }

    private void number() {
        if (isDigit(peak())) {
            advance();
        }

        if (peak() == '.' && isDigit(peakNext())) {
            advance();

            while (isDigit(peak())) {
                advance();
            }
        }

        addToken(TokenType.NUMBER,
                Double.parseDouble(source.substring(start, current)));
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
                || c == '_';
    }

    private boolean isAlphaNumeric(char c) {
        return isAlpha(c) || isDigit(c);
    }
}
