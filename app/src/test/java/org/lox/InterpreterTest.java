package org.lox;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.ArrayList;

public class InterpreterTest {

    Interpreter interpreter;
    List<Object> results;

    @BeforeEach
    void setup() {
        interpreter = new Interpreter((Object expr) -> {
            results.add(expr);
        });
    }

    @Test
    void testInterpret() {
        test("print 2.5;", "2.5");
        test("print 2.0;", "2");
        test("print (4 + 5) < 8;", "false");
        test("print (4 + 5) > 8;", "true");
        test("print (4 + 5) == 9;", "true");
        test("print 5 + \"yo\";", "5yo");
        test("print 15 + (10 / 2);", "20");
        test("print 2; print \"hello\";", List.of("2", "hello"));
        test("print 15 + (10 / 2);", "20");
        test("var test = 5; print test;", "5");

        test("""
                // A comment
                    print "hello";

                /* a multi
                line
                comment */ print "yo";
                    """, List.of("hello", "yo"));

        test("""
                    var test = 5;
                    test = 20;
                    print test;
                """, "20");

        test("""
                    var a = 2;
                    {
                        var a = 20;
                        print a;
                    }
                    print a;

                    var b = 20;
                    {
                        print b;
                    }

                    var c = 2;
                    {
                        c = 20;
                        print c;
                    }
                    print c;

                    var d = 50;
                    {
                        var d = 5;
                        d = 1;
                        print d;
                    }
                    print d;


                    {
                    }
                """, List.of("20", "2", "20", "20", "20", "1", "50"));

        test("var a = 2; print a = 5;", "5");

        test("""
                    if (8 > 5) print "true!";

                    if (false) {
                        print "true 2";
                    } else {
                        print "false 2";
                    }

                    if (false) {
                        print "true 3";
                    }
                """, List.of("true!", "false 2"));

        test("""
                if (true or false) print "true or 1";

                if (false or true) print "true or 2";

                if (false or false) print "true or 3";

                if (true and true) print "true and 1";

                if (true and false) print "true and 2";
                """, List.of("true or 1", "true or 2", "true and 1"));

        test("""
                var i = 0;

                loop (i < 3) {
                    i = i + 1;
                    print "loop " + i;
                }
                """, List.of("loop 1", "loop 2", "loop 3"));
    }

    void test(String expression, Object expected) {
        test(expression, List.of(expected));
    }

    void test(String expression, List<Object> expected) {
        results = new ArrayList<Object>();

        var scanner = new Scanner(expression);
        var tokens = scanner.scanTokens();
        var parser = new Parser(tokens);
        var expr = parser.parse();

        interpreter.interpret(expr);

        assertEquals(expected, results);
    }
}
