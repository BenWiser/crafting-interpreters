package org.lox;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class AstPrinterTest {
    @Test
    void testPrinting() {
        var expression = new Expr.Binary(
                new Expr.Unary(
                        new Token(TokenType.MINUS, "-", null, 1),
                        new Expr.Literal(123)),
                new Token(TokenType.STAR, "*", null, 1),
                new Expr.Grouping(
                        new Expr.Literal(45.67)));

        var expected = "(* (- 123) (group 45.67))";
        var actual = new AstPrinter().print(expression);

        assertEquals(expected, actual);
    }
}
