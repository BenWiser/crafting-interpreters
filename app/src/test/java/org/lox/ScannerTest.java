package org.lox;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

class ScannerTest {
    @Test
    void testSimple() {
        var scanner = new Scanner("""
                // Nothing!
                ( ) * /
                != ! 8.412
                "A string!"
                """);
        var expected = tokens(
                new Token(TokenType.LEFT_PAREN, "(", null, 1),
                new Token(TokenType.RIGHT_PAREN, ")", null, 1),
                new Token(TokenType.STAR, "*", null, 1),
                new Token(TokenType.SLASH, "/", null, 1),
                new Token(TokenType.BANG_EQUAL, "!=", null, 2),
                new Token(TokenType.BANG, "!", null, 2),
                new Token(TokenType.NUMBER, "8.412", 8.412, 2),
                new Token(TokenType.STRING, "\"A string!\"", "A string!", 3),
                EOF(4));

        var actual = scanner.scanTokens();
        assertIterableEquals(expected, actual);
    }

    @Test
    void testMultilineComments() {
        var scanner = new Scanner("""
                (
                ! /* ada
                hello foo
                */ *
                """);
        var expected = tokens(
                new Token(TokenType.LEFT_PAREN, "(", null, 0),
                new Token(TokenType.BANG, "!", null, 1),
                new Token(TokenType.STAR, "*", null, 3),
                EOF(4));

        var actual = scanner.scanTokens();
        assertIterableEquals(expected, actual);
    }

    @Test
    void testIdentifers() {
        var scanner = new Scanner("""
                and or var
                hello foo
                loop
                """);

        var expected = tokens(
                new Token(TokenType.AND, "and", null, 0),
                new Token(TokenType.OR, "or", null, 0),
                new Token(TokenType.VAR, "var", null, 0),
                new Token(TokenType.IDENTIFIER, "hello", null, 1),
                new Token(TokenType.IDENTIFIER, "foo", null, 1),
                new Token(TokenType.LOOP, "loop", null, 2),
                EOF(3));

        var actual = scanner.scanTokens();
        assertIterableEquals(expected, actual);
    }

    List<Token> tokens(Token... tokens) {
        return List.of(tokens);
    }

    Token EOF(int line) {
        return new Token(TokenType.EOF, "", null, line);
    }
}
